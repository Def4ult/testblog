import { PrismaClient } from '@prisma/client'
import {Request, Response} from 'express'

const prisma = new PrismaClient()

export default {
  async index(req: Request, res: Response){
    const users = await prisma.user.findMany()
    res.json(users)
  },

  async create(req: Request, res: Response){
    console.log(req)
    const result = await prisma.user.create({
      data: { ...req.body },
    })
    res.json(result)
  },

  async update(req: Request, res: Response){
    const {name, email} = req.body
    const {id} = req.params
    const result = await prisma.user.update({
      where: {id: Number(id)},
      data: {
        name: name,
        email: email
      }
    })
    res.json(result)
  }

};