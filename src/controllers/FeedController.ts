import { PrismaClient } from '@prisma/client'
import {Request, Response} from 'express'

const prisma = new PrismaClient()

export default{

  async index(req: Request, res: Response){
    const posts = await prisma.post.findMany({
      where: { published: true },
      include: { author: true }
    })
    res.json(posts)
  }
};