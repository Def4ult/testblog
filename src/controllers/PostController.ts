import { PrismaClient } from '@prisma/client'
import {Request, Response} from 'express'

const prisma = new PrismaClient()

export default{

  async index(req: Request, res: Response){
    const { id } = req.params
    const post = await prisma.post.findUnique({
      where: { id: Number(id) },
    })
    res.json(post)
  },

  async create(req: Request, res: Response){
    const { title, content, authorEmail } = req.body
    const result = await prisma.post.create({
      data: {
        title,
        content,
        published: false,
        author: { connect: { email: authorEmail } },
      },
    })
    res.json(result)
  },

  async update(req: Request, res: Response){
    const { id } = req.params
    const post = await prisma.post.update({
      where: { id: Number(id) },
      data: { published: true },
    })
    res.json(post)
  },

  async destroy(req: Request, res: Response){
    const { id } = req.params
    const post = await prisma.post.delete({
      where: { id: Number(id) },
    })
    res.json(post)
  }
};