import UserController from './controllers/UserController'
import FeedController from './controllers/FeedController'
import PostController from './controllers/PostController'

const express = require('express');

const routes = express.Router();

routes.get('/user', UserController.index)
routes.post('/user', UserController.create)
routes.put('/user/:id', UserController.update)

routes.get('/feed', FeedController.index)

routes.get('/post/:id', PostController.index)
routes.post('/post', PostController.create)
routes.put('/post/publish/:id', PostController.update)
routes.delete('/post/:id', PostController.destroy)

export default routes;
